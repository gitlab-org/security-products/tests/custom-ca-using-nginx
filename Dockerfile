FROM nginx:latest

COPY src /etc/nginx/html
COPY nginx-localhost.crt /etc/ssl/certs/nginx-localhost.crt
COPY nginx-localhost.key /etc/ssl/private/nginx-localhost.key
COPY nginx-maven.crt /etc/ssl/certs/nginx-maven.crt
COPY nginx-maven.key /etc/ssl/private/nginx-maven.key
COPY nginx-dot-net.crt /etc/ssl/certs/nginx-dot-net.crt
COPY nginx-dot-net.key /etc/ssl/private/nginx-dot-net.key
COPY ssl.conf /etc/nginx/conf.d/ssl.conf
COPY dhparam.pem /etc/ssl/certs/dhparam.pem
